import React from "react";
import { Text } from "react-native";
import Routes from "./Routes";
import { createAppContainer } from "react-navigation";

const AppContainer = createAppContainer(Routes);
export default class Main extends React.Component {
  render() {
    return <AppContainer />;
  }
}
