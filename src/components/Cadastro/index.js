/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Dimensions,
  TouchableOpacity
} from "react-native";

const { width } = Dimensions.get("window");

export default class Cadastro extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    nome: "",
    email: "",
    senha: "",
    telefone: ""
  };
  navigationToList = () => {};

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>CADASTRO DE CLIENTES</Text>
        <TextInput
          style={styles.txInput}
          onChangeText={nome => this.setState({ nome: nome })}
          value={this.state.nome}
          placeholder="NOME"
        />
        <TextInput
          style={styles.txInput}
          placeholder="EMAIL"
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          style={styles.txInput}
          onChangeText={senha => this.setState({ senha })}
          placeholder="SENHA"
          value={this.state.senha}
        />
        <TextInput
          style={styles.txInput}
          onChangeText={telefone => this.setState({ telefone })}
          placeholder="TELEFONE"
          value={this.state.telefone}
        />
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("Listagem")}
        >
          <Text style={styles.btn}> SALVAR </Text>
        </TouchableOpacity>

        <View style={styles.retorno}>
          <Text>{this.state.nome}</Text>
          <Text>{this.state.email}</Text>
          <Text>{this.state.telefone}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#362986"
  },
  header: {
    fontSize: 24,
    color: "#fff",
    marginTop: 22
  },
  txInput: {
    backgroundColor: "#94D211",
    height: 40,
    marginTop: 24,
    width: width - 30,
    borderRadius: 10,
    fontSize: 18,
    paddingLeft: 20
  },
  btn: {
    height: 52,
    backgroundColor: "#8377C8",
    marginTop: 30,
    borderRadius: 10,
    color: "#fff",
    fontSize: 24,
    width: width - 30,
    textAlign: "center",
    paddingTop: 10
  },

  retorno: {
    backgroundColor: "#fff",
    width: width - 30,
    padding: 20,
    fontSize: 16,
    marginTop: 24
  }
});
