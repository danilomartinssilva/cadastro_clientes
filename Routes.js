import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import Cadastro from "./src/components/Cadastro";
import Listagem from "./src/components/Listagem";

const AppNavigator = createStackNavigator(
  {
    Cadastro: {
      screen: Cadastro
    },
    Listagem: {
      screen: Listagem
    }
  },
  {
    initialRouteName: "Cadastro"
  }
);

export default createAppContainer(AppNavigator);
